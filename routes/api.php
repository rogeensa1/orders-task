<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\OrderApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
Route::group(
[
    'prefix' => 'api',
] ,function () {

        Route::get('/loginApi', [AuthController::class, 'login']);

        Route::group(
        [
            'middleware' => ['auth:sanctum', 'verified','admin']
        ] ,function () {
            Route::get('/order/list', [OrderApiController::class, 'getAllOrders']);
        });
});*/

//Route::get('/loginApi',[AuthController::class, 'login']);

//Route::middleware('auth:sanctum')->get('/order/list',[OrderApiController::class, 'getAllOrders']);
