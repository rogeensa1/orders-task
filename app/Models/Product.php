<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function order(){
        return $this -> belongsTo('App\Models\Order','customer_id','id');
    }

    protected $fillable = ['photo','title','price','order_id'];


}
