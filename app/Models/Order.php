<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function customer(){
        return $this -> belongsTo('App\Models\User','user_id','id');
    }

    public function products(){
        return $this->hasMany('App\Models\Product', 'order_id', 'id');
    }

    protected $casts = [
        'created_at' => 'date:Y-m-d',
    ];
}
