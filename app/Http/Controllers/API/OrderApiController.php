<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Models\User;
use App\Models\Order;

class OrderApiController extends Controller
{
    public function getAllOrders()
    {
        $orders = Order::with(['customer','products'])->get();
        return response()
            ->json([
                     'code'=>200,
                     'orders' => $orders,
                  ]);
    }

}
