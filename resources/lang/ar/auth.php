<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'بيانات الاعتماد هذه لا تتطابق مع سجلاتنا.',
    'password' => 'كلمة المرور غير صحيحة.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    "TitleRequiredField"=>'حقل العنوان مطلوب!',
    "MoreRequiredField"=>'حقل المزيد مطلوب!',
    "ImageRequiredField"=>'حقل الصورة مطلوب!',
    "ExtentionPhotoValidation"=>'لاحقة الصورة مرفوضة',

    "TitleMustBeString"=>"يجب على العنوان ان يكون محارف",
    "TitleMaxCaracter"=>"اعلى عدد احرف هو  :number",
    "DescMinCaracter"=>"أقل عدد احرف للوصف هو  :number",
    "DescRequiredField"=>"حقل الوصف مطلوب"

];
