<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="{{session()->get('direction')}}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="userId" content="{{ auth()->check() ? auth()->id() : ' ' }}">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="{{ asset('/admin/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/admin/images/ico/favicon.ico')}}">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    @if (session()->get('direction') == "rtl")
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/vendors.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/plugins/extensions/toastr.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/admin/css-rtl/custom-rtl.css')}}">

        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/core/menu/menu-types/vertical-content-menu.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/core/colors/palette-gradient.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/admin/css/style-rtl.css')}}">
    @else
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/vendors.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/plugins/extensions/toastr.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/app.css')}}">

        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/core/menu/menu-types/vertical-content-menu.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/core/colors/palette-gradient.css')}}">
    @endif
    <!-- END VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/vendors/css/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/vendors/css/extensions/sweetalert.css')}}">

    <!-- BEGIN MODERN CSS-->

    <!-- END MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">

    <!-- BEGIN Page Level CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/fonts/simple-line-icons/style.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/vendors/css/extensions/sweetalert.css')}}">

    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/style.css')}}">
    <!-- END Custom CSS-->
    <style>
        .table th, .table td{
            padding: 0.75rem 2rem;
        }
        .page-item.active .page-link{
            border: 0;
            color: #fff;
            background-color : #BA68C8;
            margin: 0 6px;
        }
        .page-item.disabled .page-link{
            border: 0
        }
    </style>
    @yield('style')
</head>

<body class="vertical-layout vertical-content-menu 2-columns   menu-expanded fixed-navbar" data-open="click"
data-open="click" data-menu="vertical-content-menu" data-col="2-columns">

    @include('admin.include.navbar')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            @include('admin.include.sidebar')
            <div class="content-body">
            @yield('content')
            </div>
        </div>
    </div>
    @include('admin.include.footer')
    <script>
    </script>
    <script src="{{ asset('/admin/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/admin/data/jvector/visitor-data.js')}}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ asset('admin/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/admin/js/core/app.js')}}" type="text/javascript"></script>
    {{-- <script src="{{ asset('/admin/js/scripts/customizer.js')}}" type="text/javascript"></script> --}}
    <script src="{{asset('admin/vendors/js/ui/headroom.min.js')}}" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('admin/vendors/js/tables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/admin/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('/admin/vendors/js/extensions/sweetalert.min.js')}}"></script>
    <script>
    var url = "http://127.0.0.1:5000/";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
    @yield('script')
</body>
</html>
