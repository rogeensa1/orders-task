@extends('layouts.admin',[
'activePage' => 'order',
])
@section('title')
    Add
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('/admin/vendors/css/forms/selects/select2.min.css') }}">
@endsection
@section('content')
    <section class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php // dd($entries) ?>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <p><strong>Opps Something went wrong</strong></p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{ route('admin.order.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row mt-1">
                                <div class="col-md-6">
                                
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="form-group col-12 mb-2">
                                                <label for="name">Name</label>
                                                <input type="text" id="name" class="form-control " name="name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="row">
                                            <div id="user-container" class="form-group  col-12 mb-2">
                                                <label for="user">User</label>
                                                <select type="text" id="user" class="form-control " name="user">
                                                    @foreach ($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions clearfix">
                                        <div class="buttons-group float-right">
                                            <a href="{{route('admin.order')}}"  class="btn btn-warning cancel btn-sm mr-1">
                                                Cancel
                                            </a>
                                            <button type="submit"
                                                class="btn text-white bg-cyan bg-darken-12 btn-sm mr-1">
                                                Save
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
<script src="{{ asset('/admin/vendors/js/forms/select/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/admin/js/scripts/forms/select/form-select2.min.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        $('#user').select2({ dropdownParent: $("#user-container") }).trigger('change');
    });
</script>
@endsection
