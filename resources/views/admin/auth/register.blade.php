@extends('layouts.auth')

@section('content')

<div class="col-md-4 col-10 box-shadow-2 p-0">
    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
      <div class="card-header border-0">
       

      </div>
      <div class="card-content">

        <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
            <span>{{ __('Register') }}</span>
          </p>
        <div class="card-body">
          <form class="form-horizontal" action="{{ route('register') }}" method="post" novalidate>
            @csrf
            <fieldset class="form-group position-relative has-icon-left">


                <input id="text" type="name" placeholder="{{ __('Name') }}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                  @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                <div class="form-control-position">
                  <i class="ft-user"></i>
                </div>
              </fieldset>
            <fieldset class="form-group position-relative has-icon-left">

              <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              <div class="form-control-position">
                <i class="ft-mail"></i>
              </div>
            </fieldset>
            <fieldset class="form-group position-relative has-icon-left">
                <input required id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password') }}" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

              <div class="form-control-position">
                <i class="la la-key"></i>
              </div>
            </fieldset>

            <fieldset class="form-group position-relative has-icon-left">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

              <div class="form-control-position">
                <i class="la la-key"></i>
              </div>
            </fieldset>




            <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i> {{ __('Register') }}</button>
            </div>

            <div class="card-body">
                <a href="{{url('/login')}}" class="btn btn-outline-danger btn-block"><i class="ft-unlock"></i> {{ __('Login') }}</a>
              </div>
          </form>
        </div>

      </div>
    </div>
  </div>

@endsection



