<div class="main-menu menu-static menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class=" {{  ($activePage == 'dashboard') ? 'open nav-item' : 'nav-item'}}  "><a
                    href="{{ route('dashboard') }}"><i class="la la-home"></i><span class="menu-title">dashboard</span></a>
            </li>


            <li class=" {{  ($activePage == 'order') ? 'open nav-item' : 'nav-item'}}  "><a
                    href="{{ route('admin.order') }}"><i class="ft-image"></i><span class="menu-title">Orders</span></a>
            </li>

            {{-- <li class=" {{  ($activePage == 'product') ? 'open nav-item' : 'nav-item'}}  "><a
                href="{{ route('admin.product') }}"><i class="ft-image"></i><span class="menu-title">Products</span></a>
        </li> --}}

        </ul>
    </div>
</div>
