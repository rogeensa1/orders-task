@extends('layouts.admin',[
'activePage' => 'slide',
])
@section('title')
Order Products
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('/admin/vendors/js/gallery/photo-swipe/photoswipe.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{asset('/admin/vendors/js/gallery/photo-swipe/default-skin/default-skin.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/admin/css/pages/gallery.css')}}">
@endsection
@section('content')
<section class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Order Products</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                    <a href="{{ route('admin.orderProducts.create',$id) }}" class="btn btn-primary btn-sm"><i
                            class="ft-plus white"></i>Add</a>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.include.message')
                    <div class="card-body">
                        <div class="card-deck-wrapper">
                            <div class="">
                                <div class="row">
                                    @foreach ($products as $product)
                                    <div class="col-md-6">
                                        <figure class="effect-zoe card card-img-top border-grey border-lighten-2"
                                        itemprop="associatedMedia" itemscope
                                        itemtype="http://schema.org/ImageObject">
                                        <div class="overlay">
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                <a class=" btn text-white g-accent-4 "
                                                    href="{{ route('admin.orderProducts.edit', $product->id) }}"><i
                                                        class="ft-edit-2"></i></a>
                                                <a href="{{ route('admin.orderProducts.delete', $product->id) }}"
                                                    class="delete  btn text-white  bg-lighten-1 "><i
                                                        class="ft-trash-2"></i></a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <a href="{{asset('images/'.$product->photo)}}"
                                                itemprop="contentUrl" data-size="480x360">
                                                <img class="gallery-thumbnail card-img-top"
                                                    src="{{asset('images/'.$product->photo)}}"
                                                    itemprop="thumbnail" alt="Image description" />
                                            </a>
                                            <div class="card-body">
                                                <h4 class="card-title">{{$product->title}}</h4>
                                                <h4 class="card-text">{{$product->price}}</h4>
                                            </div>

                                        </figure>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script src="{{asset('/admin/vendors/js/gallery/masonry/masonry.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/vendors/js/gallery/photo-swipe/photoswipe.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js')}}" type="text/javascript">
</script>
<script src="{{asset('/admin/js/scripts/customizer.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/scripts/gallery/photo-swipe/photoswipe-script.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
            $('.delete').click(function (e) {
            e.preventDefault();
            swal({
                title: "Are u sure",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Yes",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(isConfirm => {
                if (isConfirm) {
                    window.location = $(this).attr('href')
                } else {
                    swal("Procee Canceled", "Done");
                }

            });
        });


        });
</script>
@endsection
