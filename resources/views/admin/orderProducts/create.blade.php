@extends('layouts.admin',[
'activePage' => 'product',
])

@section('title')
{{ 'Add' }}
@endsection

@section('content')

<section class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Add</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <p><strong>{{ 'Opps Something went wrong' }}</strong></p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.orderProducts.store') }}">
                        @csrf
                        <div class="row mt-1">
                            <input type="hidden" name="order_id" value="{{$id}}">
                            <div class="col-md-6">

                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group col-12 mb-2">
                                            <label for="image">Image</label>
                                            <input type="file" id="image" class="form-control " name="photo">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group col-12 mb-2">
                                            <label for="title">Title</label>
                                            <input type="text" id="title" class="form-control " value="{{old('title')}}" name="title">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group col-12 mb-2">
                                            <label for="title">Price</label>
                                            <input type="text" id="price" class="form-control " value="{{old('price')}}" name="price">
                                        </div>
                                    </div>
                                </div>



                                <div class="form-actions clearfix">
                                    <div class="buttons-group float-right">

                                        <button type="submit"
                                            class="btn text-white bg-cyan bg-darken-12 btn-sm mr-1">
                                             Save
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <img src="" class="w-100" id="preview" alt="">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script>
        $(document).ready(function() {

    $('#image').change(function (e) {
        e.preventDefault();
        var path = URL.createObjectURL(e.target.files[0]);
        $('#preview').attr('src',path)
      });

});

    </script>
@endsection
